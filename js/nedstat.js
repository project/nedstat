(function($) {
  $(document).ready(function(){
    //hide nodetype counternames
    $('#nedstat-countername-node-type-settings').hide();

    //set token-settings visible when selected
    if ($("#edit-nedstat-general-countername-generate-type input:radio:checked").val() == 'tokens') {
      $('#nedstat-countername-node-type-settings').show();
    }

    //toggle settings
    $('#edit-nedstat-general-countername-generate-type input:radio').bind('change', function() {
      if ($(this).val() == 'tokens') {
        $('#nedstat-countername-node-type-settings').show();
      }
      else{
        $('#nedstat-countername-node-type-settings').hide();
      }
    });

  });
})(jQuery);